# README #

Este Readme contiene informacion sobre el armado de una incubadora de aves.

### Descripción general del prototipo ###

El proyecto se basa en crear una incubadora que permite el crecimiento de huevos de tres tipos de aves: Gallina, Codorniz y Pato.
La incubadora tiene control de temperatura y humedad para asegurar las condiciones de vida correctas para cada tipo de animal. Consta de pulsadores y distintos tipos de alarmas visuales o audibles que permiten al usuario controlar y conocer el estado de la incubadora, además para poder obtener información más precisa y detallada de todo lo que ocurre dentro de la incubadora, puede conectarse por puerto serie a una pc.
El alma de este proyecto es la placa LPC1769 revD. La misma es totalmente funcional para este proyecto debido a que además de ser utilizada para procesar toda la información requerida, con su potente cortex M3, también posee una serie de periféricos que serán de gran ayuda a la hora de obtener, muestrear y mostrar dicha información.

### Colaboradores ###

* Octavio Sosa  
* Joel Wayar
* Macarena Gauna 