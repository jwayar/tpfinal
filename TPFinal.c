/*
 ============================================================================
 Name		 : TPFinal.c
 Author		 : Octavio Sosa, Macarena Gauna, Joel Wayar
 Version	 : 0
 Copyright	 :
 Description : INCUBADORA DE HUEVOS
 ============================================================================
 */

#include "TPFinal.h"


//----------Programa Principal----------//

int main(void){

	SystemInit();
	reset_high = 1;	//Comienza en 1 para que entre al while la primera vez

	while(reset_high){
		//Inicialización de variables
		reset_high = 0;
		conv_done = 0;			//Inicializo flag de conversion del ADC
		dias_incubacion  = 0;
		horas_incubacion = 0;
		configTypeIncubadora(HUEVO_GALLINA); //seteamos la incubadora para HUEVO_GALLINA

		confPin();
		confADC();
		confUart();
		confTIMER0();
		confTIMER1();

		while (!reset_high){			//Se queda dando vueltas indefinidamente hasta que se toca el boton de reset_high

			if (conv_done) {			//Entro a este if cuando se realiza la conversion (cada 30 seg)
				hand_temp();			//Manejo de temperatura
				hand_humedad();			//Manejo de humedad
			}

		}

		ADC_DeInit(LPC_ADC); //Desinicializa ADC
		TIM_DeInit(LPC_TIM0);//Desinicializar uart tambien
		TIM_DeInit(LPC_TIM1);
		UART_DeInit(LPC_UART0);
	}

	return 0;
}



//-----------Funciones-----------//

void configTypeIncubadora(uint8_t TipoDeHuevo){
	switch (TipoDeHuevo){ // HUEVO_GALLINA ; HUEVO_CODORNIZ ; HUEVO_PATO
		case 0:
			//- HUEVO_GALLINA
			Incubadora_CFG.Huevo 		= HUEVO_GALLINA; 		// Tipo de Huevo
			Incubadora_CFG.TiempoInc	= 23;			 		// Tiempo de incubación en dias,
			Incubadora_CFG.Temperatura	= 37.5; 				// Temperatura en grados centígrados
			Incubadora_CFG.TiempoRotaciones = 6;				// Tiempo entre rotaciones (en Horas)
		break;

		case 1:
			//- HUEVO_CODORNIZ
			Incubadora_CFG.Huevo 		= HUEVO_CODORNIZ; 		// Tipo de Huevo
			Incubadora_CFG.TiempoInc	= 18;			 		// Tiempo de incubación en dias,
			Incubadora_CFG.Temperatura	= 37.9;					// Temperatura en grados centígrados
			Incubadora_CFG.TiempoRotaciones = 6;				// Tiempo entre rotaciones (en Horas)
		break;

		case 2:
			//- HUEVO_PATO
			Incubadora_CFG.Huevo		= HUEVO_PATO; 			// Tipo de Huevo
			Incubadora_CFG.TiempoInc	= 31; 					// Tiempo de incubación en dias,
			Incubadora_CFG.Temperatura	= 20.5; 				// Temperatura en grados centígrados
			Incubadora_CFG.TiempoRotaciones =8;					// Tiempo entre rotaciones (en Horas)
		break;
	}

	Incubadora_CFG.Humedad	= 60;

	return;
}




/*
@input:
P0.23 como AD0.0 Temperatura
P0.24 como AD0.1 Humedad
P2.10 como EINT0 Pulsador de seleccion
P2.11 como EINT1 Pulsador de reset_high

@outputs activas por alto:
P2.2 como GPIO0 Lampara (relé)
P2.3 como GPIO0 Led indicador de humedad
P2.4 como GPIO0 Motor Giro
P2.5 como GPIO0 Led Alarma se viene Motor Giro
P2.6 como GPIO0 Led Alarma TiempoIncubacion completo

*/
void confPin(void){
	PINSEL_CFG_Type PinCfg;

	//---------------------------------
	//INPUT:select P0.23 como AD0.0 -> Temperatura
	PinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	PinCfg.Funcnum 	= PINSEL_FUNC_1;			// como A/D
	PinCfg.Pinmode	= PINSEL_PINMODE_TRISTATE;
	PinCfg.Portnum 	= PINSEL_PORT_0;
	PinCfg.Pinnum 	= PINSEL_PIN_23;
	PINSEL_ConfigPin(&PinCfg);

	//INPUT:select P0.24 como AD0.1 -> Humedad
	PinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	PinCfg.Funcnum 	= PINSEL_FUNC_1;			// como A/D
	PinCfg.Pinmode	= PINSEL_PINMODE_TRISTATE;
	PinCfg.Portnum 	= PINSEL_PORT_0;
	PinCfg.Pinnum 	= PINSEL_PIN_24;
	PINSEL_ConfigPin(&PinCfg);

	//INPUT:PULSADOR select P2.10 como EINT0 (pulsador de seleccion)
	PinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	PinCfg.Funcnum 	= PINSEL_FUNC_1;			// EINT0
	PinCfg.Pinmode	= PINSEL_PINMODE_PULLUP;
	PinCfg.Portnum 	= PINSEL_PORT_2;
	PinCfg.Pinnum 	= PINSEL_PIN_10;
	PINSEL_ConfigPin(&PinCfg);
	NVIC_EnableIRQ(EINT0_IRQn);					//Habilito interrupcion por EINT0

	//INPUT:PULSADOR select P2.11 como EINT1 (pulsador de reset_high)
	PinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	PinCfg.Funcnum 	= PINSEL_FUNC_1;			// EINT1
	PinCfg.Pinmode	= PINSEL_PINMODE_PULLUP;
	PinCfg.Portnum 	= PINSEL_PORT_2;
	PinCfg.Pinnum 	= PINSEL_PIN_11;
	PINSEL_ConfigPin(&PinCfg);
	NVIC_EnableIRQ(EINT1_IRQn);					//Habilito interrupcion por EINT1

	//---------------------------------
	//OUTPUT:LÁMPARAS select P2.2 como GPIO0 (Activa el rele que prende las lámparas)
	PinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	PinCfg.Funcnum 	= PINSEL_FUNC_0;			//GPIO
	PinCfg.Pinmode	= PINSEL_PINMODE_TRISTATE;
	PinCfg.Portnum 	= PINSEL_PORT_2;
	PinCfg.Pinnum 	= PINSEL_PIN_2;
	PINSEL_ConfigPin(&PinCfg);
	GPIO_SetDir( 	 PUERTO_(2), PIN_(2),	SALIDA); 	//P2.2 como Output
	GPIO_ClearValue( PUERTO_(2), PIN_(2)); 				//P2.2 = 0;	Las lámparas comienzan apagadas

	//OUTPUT:LED Humedad select P2.3 como GPIO0 (indicador de humedad)
	PinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	PinCfg.Funcnum 	= PINSEL_FUNC_0;
	PinCfg.Pinmode	= PINSEL_PINMODE_TRISTATE;
	PinCfg.Portnum 	= PINSEL_PORT_2;
	PinCfg.Pinnum 	= PINSEL_PIN_3;
	PINSEL_ConfigPin(&PinCfg);
	GPIO_SetDir( 	 PUERTO_(2), PIN_(3),	SALIDA);
	GPIO_ClearValue( PUERTO_(2), PIN_(3));

	//OUTPUT:Motor select P2.4 como GPIO0
	PinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	PinCfg.Funcnum 	= PINSEL_FUNC_0;
	PinCfg.Pinmode	= PINSEL_PINMODE_TRISTATE;
	PinCfg.Portnum 	= PINSEL_PORT_2;
	PinCfg.Pinnum 	= PINSEL_PIN_4;
	PINSEL_ConfigPin(&PinCfg);
	GPIO_SetDir( 	 PUERTO_(2), PIN_(4),	SALIDA);
	GPIO_ClearValue( PUERTO_(2), PIN_(4));

	//OUTPUT:LED Motor select P2.5 como GPIO0 (LED alarma de motor)
	PinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	PinCfg.Funcnum 	= PINSEL_FUNC_0;
	PinCfg.Pinmode	= PINSEL_PINMODE_TRISTATE;
	PinCfg.Portnum 	= PINSEL_PORT_2;
	PinCfg.Pinnum 	= PINSEL_PIN_5;
	PINSEL_ConfigPin(&PinCfg);
	GPIO_SetDir( 	 PUERTO_(2), PIN_(5),	SALIDA);
	GPIO_ClearValue( PUERTO_(2), PIN_(5));

	//OUTPUT:LED TiempoIncubacion select P2.6 como GPIO0 (LED alarma TiempoIncubacion completo)
	PinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	PinCfg.Funcnum 	= PINSEL_FUNC_0;
	PinCfg.Pinmode	= PINSEL_PINMODE_TRISTATE;
	PinCfg.Portnum 	= PINSEL_PORT_2;
	PinCfg.Pinnum 	= PINSEL_PIN_6;
	PINSEL_ConfigPin(&PinCfg);
	GPIO_SetDir( 	 PUERTO_(2), PIN_(6),	SALIDA);
	GPIO_ClearValue( PUERTO_(2), PIN_(6));

	// to test: (GPIO por defecto)								/////Esto hay que sacarlo ???????????????
	GPIO_SetDir(   PUERTO_(0), PIN_(22),	SALIDA);
	GPIO_SetValue( PUERTO_(0), PIN_(22));

	//UART
	//configuración pin de Tx
	PinCfg.Funcnum = PINSEL_FUNC_1;
	PinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	PinCfg.Pinmode = PINSEL_PINMODE_PULLUP;
	PinCfg.Pinnum = PINSEL_PIN_2;
	PinCfg.Portnum = PINSEL_PORT_0;
	PINSEL_ConfigPin(&PinCfg);
	//config de pin match0.0
	PinCfg.Funcnum 	= PINSEL_FUNC_3;
	PinCfg.Portnum 	= PINSEL_PORT_1;
	PinCfg.Pinnum 	= PINSEL_PIN_28;
	PINSEL_ConfigPin(&PinCfg);

	return;
}



void confADC(void){
	/* Configuration for ADC :
	* Select: ADC channel 0 (Temperatura)
	* Select: ADC channel 1 (Humedad)
	* ADC conversion rate = 200KHz
	* ADC in burst mode
	* ADC start conversion in Timer_Handler
	*/
	ADC_Init(LPC_ADC, 200000);
	// Chanel0: Temperatura
	ADC_IntConfig(LPC_ADC, ADC_ADINTEN0,SET);
	ADC_ChannelCmd(LPC_ADC,SENSOR_TEMP,SET);
	//Chanel1: Humedad
	ADC_IntConfig(LPC_ADC, ADC_ADINTEN1,SET);
	ADC_ChannelCmd(LPC_ADC,SENSOR_HUM,SET);

	ADC_StartCmd(LPC_ADC, ADC_START_CONTINUOUS);	//Para modo burst
	ADC_BurstCmd(LPC_ADC, DISABLE);					//Empieza con el modo burst desactivado
	NVIC_DisableIRQ(ADC_IRQn);						//Empieza con con las interrupciones desactivadas
	return;
}



void confTIMER0 (void){
	/* Configuramos el MAT0.1 del Timer0 para que
	 * interrumpa cada 30 segundos (para activar el ADC).
	 * */

	//Configuración del TIMER
	TIM_TIMERCFG_Type conf_timer0;
	conf_timer0.PrescaleOption = TIM_PRESCALE_USVAL; 	//Prescaler en microsegundos
	conf_timer0.PrescaleValue = (uint32_t) 1000;		//valor del prescaler = 1 mS
	TIM_Init(LPC_TIM0, TIM_TIMER_MODE,  &conf_timer0);	//Habilita el modulo | PCLK_TIMER0=CCLK/4

	//Configuración de Match1 (ADC)
	TIM_MATCHCFG_Type conf_channel;
	conf_channel.MatchChannel			= 1;
	conf_channel.IntOnMatch				= ENABLE;
	conf_channel.ResetOnMatch			= ENABLE;
	conf_channel.StopOnMatch			= DISABLE;
	conf_channel.ExtMatchOutputType		= TIM_EXTMATCH_NOTHING;
	conf_channel.MatchValue				= 6000 ;//(30000);				// El match se va a producir cada: PrescaleValue*MatchValue= 30 segundos
	TIM_ConfigMatch(LPC_TIM0, &conf_channel);

	//Configuración de Match0 (UART)
	TIM_MATCHCFG_Type conf_channel0;
	conf_channel0.ExtMatchOutputType 	= TIM_EXTMATCH_NOTHING;
	conf_channel0.MatchChannel			= 0;
	conf_channel0.IntOnMatch			= ENABLE;
	conf_channel0.ResetOnMatch			= DISABLE;
	conf_channel0.StopOnMatch			= DISABLE;
	conf_channel0.MatchValue			= (5000);				//El match se va a producir cada 5 seg, si el timer aumenta en 1 cada 1miliseg
																//le pongo 5000 para que me haga match a los 5 seg.
	TIM_ConfigMatch(LPC_TIM0, &conf_channel0);

	TIM_Cmd (LPC_TIM0, ENABLE);		//Empiezo a contar
	NVIC_EnableIRQ(TIMER0_IRQn);	//Activo la interrupcion por NVIC
	return;
}

void confUart(void) {
	UART_CFG_Type UARTConfigStruct;
	UART_FIFO_CFG_Type UARTFIFOConfigStruct;

	//configuración por defecto
	UART_ConfigStructInit(&UARTConfigStruct);
	//inicializa periférico
	UART_Init(LPC_UART0, &UARTConfigStruct);

	UART_FIFOConfigStructInit(&UARTFIFOConfigStruct);
	//Inicializa FIFO
	UART_FIFOConfig(LPC_UART0, &UARTFIFOConfigStruct);
	//Habilita transmisión
	UART_TxCmd(LPC_UART0, ENABLE);
	return;
}

void confTIMER1 (void){
	/* Configuramos el Timer1 para que:
	 * # controle el tiempo de funcionamiento: Motor Rotacion de Huevos
	 * # cuente los días que lleva la incubación
	 * */

	//Configuración del TIMER1 EN SEGUNDOS
	TIM_TIMERCFG_Type conf_timer1;
	conf_timer1.PrescaleOption = TIM_PRESCALE_USVAL; 	//Prescaler en microsegundos
	conf_timer1.PrescaleValue = (uint32_t) 1000000;		//valor del prescaler en segundos
	TIM_Init(LPC_TIM1, TIM_TIMER_MODE,  &conf_timer1);	//Habilita el modulo | PCLK_TIMER1=CCLK/4


	//------Configuración para el motor------//

	//Configuración de Match0  para Alarma "se viene rotacion de huevos":
	TIM_MATCHCFG_Type conf_channel;
	conf_channel.MatchChannel			= 0;
	conf_channel.IntOnMatch				= ENABLE;
	conf_channel.ResetOnMatch			= DISABLE;
	conf_channel.StopOnMatch			= DISABLE;
	conf_channel.ExtMatchOutputType		= TIM_EXTMATCH_NOTHING;
	conf_channel.MatchValue				= (SEG2HORA*Incubadora_CFG.TiempoRotaciones)
										  - ALARMA_MOTOR_SEGUNDOS
										  - MOTOR_ON_SEGUNDOS;
	//El match de Alarma se va a producir minutos antes de "TiempoRotaciones en hora" y antes encender motor
	TIM_ConfigMatch(LPC_TIM1, &conf_channel);

	//Configuración de Match1  para MOTOR ON:
	conf_channel.MatchChannel			= 1;
	conf_channel.IntOnMatch				= ENABLE;
	conf_channel.ResetOnMatch			= DISABLE;
	conf_channel.StopOnMatch			= DISABLE;
	conf_channel.ExtMatchOutputType		= TIM_EXTMATCH_NOTHING;
	conf_channel.MatchValue				= SEG2HORA*Incubadora_CFG.TiempoRotaciones
										  - MOTOR_ON_SEGUNDOS;
	//El match se va a producir segundos antes de "TiempoRotaciones en hora"
	TIM_ConfigMatch(LPC_TIM1, &conf_channel);

	//Configuración de Match2  para MOTOR OFF: (ROTACION LISTA)
	conf_channel.MatchChannel			= 2;
	conf_channel.IntOnMatch				= ENABLE;
	conf_channel.ResetOnMatch			= ENABLE;
	conf_channel.StopOnMatch			= DISABLE;
	conf_channel.ExtMatchOutputType		= TIM_EXTMATCH_NOTHING;
	conf_channel.MatchValue				= SEG2HORA*Incubadora_CFG.TiempoRotaciones;
	//El match se va a producir cada "TiempoRotaciones en hora" y se va a reiniciar
	TIM_ConfigMatch(LPC_TIM1, &conf_channel);


	//------Configuración para días------//
	//Configuración de Match3 para la cuenta de días:
	conf_channel.MatchChannel			= 3;
	conf_channel.IntOnMatch				= ENABLE;
	conf_channel.ResetOnMatch			= DISABLE;
	conf_channel.StopOnMatch			= DISABLE;
	conf_channel.ExtMatchOutputType		= TIM_EXTMATCH_NOTHING;
	conf_channel.MatchValue				= SEG2HORA;
	//El match se va a producir cada hora
	TIM_ConfigMatch(LPC_TIM1, &conf_channel);

	TIM_Cmd (LPC_TIM1, ENABLE);		//Empiezo a contar
	NVIC_EnableIRQ(TIMER1_IRQn);	//Habilito la interrupcón en NVIC
	return;
}



/* LM35
10mV/°C
con Vref=3,3V =3300 mV
*/
void hand_temp(void ){
	uint16_t adc_value_correct;
	float adc_milivolt;

	adc_value_correct 	= (adc_value[SENSOR_TEMP]);		//Obtengo el valor del ADC justificado
	adc_milivolt 		= (adc_value_correct*3300)/4096;	//Obtengo el Voltage del ADC
	temperatura 		= adc_milivolt/10;					//Obtengo la temperatura 		----	Confirmar funcion de la recta


	//Analiza la temperatura con un error de +/- 0.5°C
	//Si la temperatura es <= a la temp seteada -0.5°C, enciendo las lámparas
	//Si la temperatura es >= a la temp seteada +0.5°C, apago las lámparas
	//Si la temperatura es +/- 0.5°C de la temp seteada, no cambia el estado de las lámparas.
	if (temperatura <= Incubadora_CFG.Temperatura - 0.5){
		//Activa relé que enciende las lámparas
		GPIO_SetValue (PUERTO_(2) , PIN_(2));		//P2.2 = 1;
	}else if((temperatura > Incubadora_CFG.Temperatura - 0.5) && (temperatura < Incubadora_CFG.Temperatura + 0.5)){
		//No cambia la salida del pin. No cambia el estado de las lámparas
		return;
	}else if(temperatura >= Incubadora_CFG.Temperatura + 0.5){
		//Desactiva relé que enciende las lámparas
		GPIO_ClearValue (PUERTO_(2) , PIN_(2));	//P2.2 = 0;
	}

	return;

}



/* AMT1001
Vmax= 3V = 100% humedad
Vmin= 0V = 0% humedad
https://www.electrodragon.com/w/AMT1001 (SPECIFICATIONS)
*/
void hand_humedad(void){
	uint16_t adc_value_correct;
	float  adc_milivolt;

	adc_value_correct 	= (adc_value[SENSOR_HUM]);	//Obtengo el valor del ADC justificado
	adc_milivolt 		= (adc_value_correct*3300)/4096;		//Obtengo el Voltage del ADC
	humedad 			= (adc_milivolt*100)/3000;				//Obtengo la temperatura 		----	Confirmar funcion de la recta


	//Analiza la humedad con un error de +/- 5%
	//Si la humedad es <= a la temp seteada -5%, enciendo LED indicador de falta de humedad
	//Si la humedad es >= a la temp seteada +5%, apago LED indicador de falta de humedad
	//Si la humedad es +/- 5% de la temp seteada, no cambia el estado del LED.
	if (humedad <= Incubadora_CFG.Humedad - 5){
		//Enciende el LED que indica falta de humedad
		GPIO_SetValue (PUERTO_(2) , PIN_(3));		//P2.3 = 1;
	}else if((humedad > Incubadora_CFG.Humedad - 5) && (humedad < Incubadora_CFG.Humedad + 0.5)){
		//No cambia la salida del pin. No cambia el estado del LED
		return;
	}else if(humedad >= Incubadora_CFG.Humedad + 5){
		//Apaga el LED que indica falta de humedad
		GPIO_ClearValue (PUERTO_(2), PIN_(3));		//P2.3 = 0;
	}

	return;

}


//------------------------------------
//-- Function Debounce
//-------------------------------------
/* debounce "Eliminador de rebote"
 * @Input: SampleA (la primera muestra de la señal, estado del pin leído por primera vez)
 * @Output LastDebounceResult (Resultado, interpretación del pin leído)
 */
uint8_t debounce(uint8_t SampleA){
	//-- Set static variables:
	static uint8_t SampleB=0;
	static uint8_t SampleC=0;
	static uint8_t LastDebounceResult =0;

	//-- Logical Function:
	LastDebounceResult = (LastDebounceResult &&
						(SampleA || SampleB || SampleC)) ||
						(SampleA && SampleB && SampleC);
	//-- Update Sample
	SampleC=SampleB;
	SampleB=SampleA;

	return LastDebounceResult;
}

//-- Function delay
void retardo(uint16_t time){
	for (uint8_t i=0; i<time; i++); // lazo de demora
	return ;
}

void enviarMensaje(){
	 uint32_t temp_values= temperatura;
	 uint32_t hum_value  = humedad;


	 uint8_t info[] = "\n\n\rTipo de huevo: ";
	 uint8_t *buff1;
	 uint32_t size;
	 uint32_t numero= temp_values*10; //trabajo con num enteros
	 uint8_t temp[6]="";
	 uint8_t index=0;
	 uint8_t hum[4]="";
	 uint8_t dias[1]="";

	 UART_Send(LPC_UART0, info, sizeof(info), BLOCKING);
	 switch(Incubadora_CFG.Huevo){
	 case 0:
		 buff1 = "GALLINA\n\r";
		 size = 9;
		 break;
	 case 1:
		 buff1 = "CODORNIZ\n\r";
		 size = 10;
		 break;
	 default:
		 buff1 = "PATO\n\r";
		 size = 6 ;
		 break;
	 }

	temp[index]=tabla(numero/100); //convierto las centenas
	if(temp[index]=='0'){
		temp[index]=' ';
	}
	index++;
	temp[index]=tabla((numero%100)/10); //convierto las decenas
	index++;
	temp[index]='.';
	index++;
	temp[index]=tabla(((numero%100)%10));
	index++;
	temp[index]=0x27; //pongo el apostrofe (no tengo °)
	index++;
	temp[index]='C';

	UART_Send(LPC_UART0, buff1, size, BLOCKING);
	uint8_t info2[] = "Temperatura actual: ";
	UART_Send(LPC_UART0, info2, sizeof(info2), BLOCKING);
	UART_Send(LPC_UART0, temp, sizeof(temp), BLOCKING);

	uint8_t info3[] = "\n\rHumedad actual: ";
	UART_Send(LPC_UART0, info3, sizeof(info3), BLOCKING);

	index=0;
	hum[index]=tabla(hum_value/100); //convierto las centenas
	if(hum[index]=='0'){
		hum[index]=' ';
	}
	index++;
	hum[index]=tabla((hum_value%100)/10); //convierto las decenas
	index++;
	hum[index]=tabla((hum_value%100)%10); //convierto las unid
	index++;
	hum[index]='%';
	UART_Send(LPC_UART0, hum, sizeof(hum), BLOCKING);


	uint8_t info4[] = "\n\rDías restantes de incubación: ";
	UART_Send(LPC_UART0, info4, sizeof(info4), BLOCKING);
	uint8_t restantes=Incubadora_CFG.TiempoInc-dias_incubacion;
	index=0;
	dias[index]=tabla(restantes/10);
	index++;
	dias[index]=tabla(restantes%10);
	UART_Send(LPC_UART0, dias, sizeof(dias), BLOCKING);

	return;
}


uint8_t tabla(uint32_t num){
	switch(num){
	case 0:
		return '0';
	case 1:
		return '1';
	case 2:
		return '2';
	case 3:
		return '3';
	case 4:
		return '4';
	case 5:
		return '5';
	case 6:
		return '6';
	case 7:
		return '7';
	case 8:
		return '8';
	default:
		return '9';

	}
}

//----------Handlers----------//

void TIMER0_IRQHandler(void)
{
	if(TIM_GetIntStatus(LPC_TIM0, TIM_MR1_INT)){
		//Enciendo el ADC para que empiece a convertir
		ADC_BurstCmd(LPC_ADC, ENABLE);					//Activo modo burst
		NVIC_EnableIRQ(ADC_IRQn);						//Activo las int de ADC
	}

	else if(TIM_GetIntStatus(LPC_TIM0, TIM_MR0_INT)){
		enviarMensaje();
	}

	TIM_ClearIntPending(LPC_TIM0,TIM_MR0_INT); //bajo la bandera
	TIM_ClearIntPending(LPC_TIM0,TIM_MR1_INT); //bajo la bandera
	return;
}

void ADC_IRQHandler(void)
{

	if (ADC_ChannelGetStatus(LPC_ADC, SENSOR_TEMP, ADC_DATA_DONE)){
        adc_value[SENSOR_TEMP] =  ADC_ChannelGetData(LPC_ADC, SENSOR_TEMP);
    }
    if (ADC_ChannelGetStatus(LPC_ADC, SENSOR_HUM, ADC_DATA_DONE)){
        adc_value[SENSOR_HUM] =  ADC_ChannelGetData(LPC_ADC,SENSOR_HUM);

		//Desactivo el ADC
		ADC_BurstCmd(LPC_ADC, DISABLE);		//ADC deja de convertir
		NVIC_DisableIRQ(ADC_IRQn);			//Desactivo interrupciones
		conv_done = 1;						//Activo flag que indica fin de conversion del ADC
    }

}



void EINT0_IRQHandler(void)
{

	//--- Hubo Interrupción por P2.10 (pulsador modo)! -> ir a linea 469 (estalinea+12)
	static uint8_t Puls, PulsAnt;

	//Antirebote
	for( uint8_t j=0; j<3; j++)
	{
		retardo(200); //80000 for (uint8_t i=0; i<200; i++){}
		PulsAnt= ( GPIO_ReadValue( PUERTO_(2) ) & ( PIN_(10) ) );
		Puls=debounce(PulsAnt);
	}
	if(Puls==0 && PulsAnt==0)
	{
		static uint8_t cont = 0;
		cont++;					//Incrementa la variable cada vez que se toca el pulsador
		cont = cont % 3;		//La variable va de 0 a 2

		configTypeIncubadora(cont);	//Configura el tipo de huevo de la cantidad de pulsadas
	}

	// Clear Interrupt Flag EINT0
	EXTI_ClearEXTIFlag (EXTI_EINT0);

	return;
}


void EINT1_IRQHandler(void)
{
	//--- Hubo Interrupción por P2.11(pulsador reset)!-> ir a linea 496 (estalinea+11)
	static uint8_t Puls, PulsAnt;
	//Antirebote
	for(uint8_t j=0; j<3; j++)
	{
		retardo(200); //80000 for (uint8_t i=0; i<200; i++){}
		PulsAnt = ( GPIO_ReadValue( PUERTO_(2) ) & ( PIN_(11) ) );
		Puls = debounce(PulsAnt);
	}
	if(Puls==0 && PulsAnt==0)
	{
		reset_high = 1;
	}
	// Clear Interrupt Flag EINT1
	EXTI_ClearEXTIFlag (EXTI_EINT1);
	return;
}


void TIMER1_IRQHandler(void)
{
	//------Configuración para el motor------//

	// ON: Led Alarma se viene Motor en Giro
	if (TIM_GetIntStatus(LPC_TIM1, TIM_MR0_INT)== SET)
	{
		GPIO_SetValue ( PUERTO_(2), PIN_(5));//PRENDER LED_ALARMA MOTOR EN GIRO
		TIM_ClearIntPending(LPC_TIM1, TIM_MR0_INT);
	}

	// OFF: Led Alarma se viene Motor en Giro
	// ON : Motor Giro
	else if (TIM_GetIntStatus(LPC_TIM1, TIM_MR1_INT)== SET)
	{
		GPIO_ClearValue ( PUERTO_(2), PIN_(5));//APAGAR LED ALARMA MOTOR EN GIRO
		GPIO_SetValue 	( PUERTO_(2), PIN_(4));//PRENDER MOTOR GIRO HUEVOS
		TIM_ClearIntPending(LPC_TIM1, TIM_MR1_INT);
	}

	// OFF: Motor Giro
	else if (TIM_GetIntStatus(LPC_TIM1, TIM_MR2_INT)== SET)
	{
		GPIO_ClearValue ( PUERTO_(2), PIN_(4));//APAGAR MOTOR GIRO
		TIM_ClearIntPending(LPC_TIM1, TIM_MR2_INT);
	}


 	//------Configuración para días------//

 	// Contador de horas y días de incubación
 	else if (TIM_GetIntStatus(LPC_TIM1, TIM_MR3_INT)== SET)
 	{
 		horas_incubacion+=6;			//Aumento 6 a una las horas de incubación
 		//if (!(horas_incubacion%24)) {	//Cada 24 hs
 			dias_incubacion++;			//aumento la cantidad de dias de incubación
 		if(dias_incubacion>= (Incubadora_CFG.TiempoInc - 2)){
 			GPIO_SetValue ( PUERTO_(2), PIN_(6));//ACTIVAR LED TIEMPO INCUBACION TERMINADO
 		}
 		if(dias_incubacion>= (Incubadora_CFG.TiempoInc)){
 			reset_high = 1;
 		}
 		TIM_ClearIntPending(LPC_TIM1, TIM_MR2_INT);
 	}

}