/*
 ============================================================================
 Name		 : TPFinal.h
 Author		 : Octavio Sosa, Macarena Gauna, Joel Wayar
 Version	 : 0
 Copyright	 :
 Description : Incubadora de huevos
 ============================================================================
 */

#include "lpc17xx_adc.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"
#include "lpc17xx_timer.h"
#include "lpc17xx_exti.h"
#include "lpc17xx_uart.h"


#define SALIDA			(uint8_t) 	1
#define ENTRADA			(uint8_t) 	0
#define PUERTO_(x) 		(uint8_t) 	x
#define PIN_(x)			(uint32_t) ( 1<<x )

//------Defines y Macros------
#define DMA_SIZE 		(uint8_t) 	1
#define HUEVO_GALLINA	(uint8_t) 	0
#define HUEVO_CODORNIZ	(uint8_t)	1
#define HUEVO_PATO		(uint8_t)	2
#define NUM_SENSORES	(uint8_t)	2
#define SENSOR_TEMP 	(uint8_t)	0
#define SENSOR_HUM		(uint8_t)	1
#define TEST_PROGRAM	(uint8_t)	1 	// (comentar linea)No test, Programa con tiempos normales (horas,dias)
								  		// TEST: con tiempos reducidos  (segundos, minutos)

#ifdef TEST_PROGRAM //fuinciona?
//--- TEST
	#define ALARMA_MOTOR_SEGUNDOS	(uint8_t) 2 // antes de q el motor funcione se prende un led
	#define MOTOR_ON_SEGUNDOS 		(uint8_t) 1 // tiempo de funcionamiento para el motor de rotacion de huevos
	#define SEG2HORA			    (uint8_t) 1 // factor mutiplicador para pasar de seg a hora
#else
	#define ALARMA_MOTOR_SEGUNDOS	(uint8_t) 120
	#define MOTOR_ON_SEGUNDOS 		(uint8_t) 5
	#define SEG2HORA			    (uint8_t) 3600
#endif
//------Definición de Structs------
typedef struct
{
	uint8_t Huevo;				/**< Tipo de Huevo, puede ser:
								- HUEVO_GALLINA	 (0 default)
								- HUEVO_CODORNIZ (1)
								- HUEVO_PATO	 (2) */
	uint8_t TiempoInc;    		/**< Tiempo de incubación en dias,
								los valores comunes son:
								- 23 días (Gallina)
								- 18 días (Codorniz)
								- 31 días (Pato) */
    float Temperatura;			/**< Temperatura en grados centígrados,
								los valores comunes son:
								- 37.5 °C (Gallina)
								- 37.9 °C (Codorniz)
								- 20.5 °C (Pato) */
    uint8_t Humedad;			/**< Humedad relativa en porcentaje,
								los valores comunes son:
								- 60 % (Gallina)
								- 60 % (Codorniz)
								- 60 % (Pato) */
    uint8_t TiempoRotaciones;	/**< Tiempo entre rotaciones (en Horas)
								los valores comunes son:
								- 6  hs (Gallina)
								- 12 hs (Codorniz)
								- 8  hs (Pato) */
} Struct_Incubadora_CFG;



//-------Definicion de Variables Globales------
uint16_t adc_value[NUM_SENSORES];	// 16bits ya que la resolución del ADC es de 12 bits
Struct_Incubadora_CFG Incubadora_CFG;
uint8_t  conv_done;					//Flag que indica el fin de la conversion del ADC
uint8_t  dias_incubacion;			// Max: 31 dias
uint8_t  horas_incubacion; 			// Max: 24horas
uint8_t  reset_high;
float 	 temperatura;
uint8_t  humedad; // en %


//------Definición de Funciones------

void confPin(void);
void confADC(void);
void confTIMER0 (void);
void confTIMER1 (void);
void confUart(void);
void enviarMensaje(void);
void hand_temp(void);
void hand_humedad(void);
void configTypeIncubadora(uint8_t);
void retardo(uint16_t);
uint8_t debounce(uint8_t );
uint8_t tabla(uint32_t num);



void EINT0_IRQHandler(void);
void EINT1_IRQHandler(void);
void TIMER1_IRQHandler(void);
void TIMER0_IRQHandler(void);

/* Tipo de variables permitidas:
int8_t
int16_t
int32_t
int64_t

uint8_t
uint16_t
uint32_t
uint64_t
*/